<!-- Learn about this code on MDN: https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images -->

<html>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
 <body onload="draw();">
   <canvas id="canvas" width="300" height="300"></canvas>
   <div id="output"></div>
   <div style="display:none;">
     <img id="source" src="https://mdn.mozillademos.org/files/5397/rhino.jpg" width="300" height="227">
     <img id="frame" src="https://mdn.mozillademos.org/files/242/Canvas_picture_frame.png" width="132" height="150">
   </div>
 </body>
<script type="text/javascript">
	function draw() {
  

  // Draw slice
  
	for(var i = 0; i<3; i++)
   {
   $('#output').append("<canvas id='face"+i+"' width='400' height='250'>");
   var canvas = document.getElementById('face'+i);
   var ctx = canvas.getContext('2d');
   ctx.drawImage(document.getElementById('source'),
                30, 71, 104, 124, 0, 0, 250, 250);
   }
  // Draw frame
  //ctx.drawImage(document.getElementById('frame'), 0, 0);
}
</script>
</html>

